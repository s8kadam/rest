package REST;

/**
 * @author SAMRUDHI
 *
 */

import java.io.FileReader;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

public class PostalCodeReader {
	private final String POSTAL_ID = "id";
	private final String POSTAL_CODE = "postcode";
	private final String POSTAL_LAT = "latitude";
	private final String POSTAL_LONG = "longitude";

	
	public PostalLocation postalCodeDetails(String filename, String postalCode) {
	
		FileReader fileReader = null;
		PostalLocation postalLoc = null;		
				
		try {
			
			fileReader = new FileReader(filename);
			
			Iterable<CSVRecord> records = CSVFormat.EXCEL.withHeader().parse(fileReader);			
			
			for (CSVRecord record : records) {														
										
				String currPostalCode = record.get(POSTAL_CODE);
				int currPostalID = Integer.parseInt(record.get(POSTAL_ID));
					
				System.out.println("Read record (" + record + "): " + currPostalCode);
				if (currPostalCode.compareTo(postalCode) == 0  && currPostalID < 20){				
					
					postalLoc = new PostalLocation();
						
					postalLoc.setId(currPostalID);
					postalLoc.setPostcode(postalCode);
					postalLoc.setLatitude(Double.parseDouble(record.get(POSTAL_LAT)));
					postalLoc.setLongitude(Double.parseDouble(record.get(POSTAL_LONG)));
						
					break;
				}				
			}
		} catch (Exception e) {
			System.out.println("Error occurred : " + e.getMessage());
		} finally {
			try {
				fileReader.close();
			  } catch (Exception e) {				
			}
		}
		
		return postalLoc;			 
		 
	}
}
