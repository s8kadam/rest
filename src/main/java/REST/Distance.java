package REST;

/**
 * @author SAMRUDHI
 *
 */

public class Distance {
	
	private  String postal_code;
	private  String postal_code2;
	private  double latitude;
	private  double latitude2;
	private  double longitude;
	private  double longitude2;
	private  double distance;
	private  String unit;
    

    public Distance (String postal_code, String postal_code2, double latitude, double longitude, double latitude2, double longitude2, double distance, String unit) {
        this.postal_code = postal_code;
        this.postal_code2 = postal_code2;
        
        this.latitude = latitude;
        this.longitude = longitude;
        
        this.latitude2 = latitude2;       
        this.longitude2 = longitude2;
        
        this.distance = distance;
        this.unit = unit;
    }
    
	public String setPostal_Code() {
	        return postal_code;
	    }
	   
    public String getPostal_Code() {
        return postal_code;
    }
    
    public String getPostal_Code2() {
        return postal_code2;
    }
    
    public double getLatitude() {
        return latitude;
    }
    
    public double getLongitude() {
        return longitude;
    }
    
    public double getLatitude2() {
        return latitude2;
    }
    
    public double getLongitude2() {
        return longitude2;
    }
    
    public double getDistance() {
        return distance;
    }
    
    public String getUnit() {
        return unit;
    }
  
}
