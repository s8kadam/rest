package REST;

/**
 * @author SAMRUDHI
 *
 */

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class DistanceController {
	
	private static final String filename = "D:/Samruddhi/Java/Test/ukpostcodes/ukpostcodes.csv";
	
	private static final String unit = "km";
	private double distance;
			
	  		    
	    @RequestMapping("/distance")
	    public Distance distance(@RequestParam(value="postal_code") String postal_code,
	    						 @RequestParam(value="postal_code2") String postal_code2) {
	    	PostalCodeReader pcReader = new PostalCodeReader();
	    	PostalLocation coordinate = pcReader.postalCodeDetails(filename, postal_code);	
	    	
	    	double latitude = coordinate.getLatitude();
	    	double longitude = coordinate.getLongitude();
	    	
	    	PostalLocation coordinate2 = pcReader.postalCodeDetails(filename, postal_code2);
	    	
	    	double latitude2 = coordinate2.getLatitude();
	    	double longitude2 = coordinate2.getLongitude();
	    	
	    	CalculateDistance calcDistance = new CalculateDistance();
	    	
	    	distance = calcDistance.calculateDistance(latitude,longitude,latitude2,longitude2);
	    	
	        return new Distance(postal_code, postal_code2, latitude, longitude, latitude2, longitude2, distance, unit);
	    }
 

	
}

